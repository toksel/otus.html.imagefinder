﻿using System.Collections.Generic;

namespace otus.html.imagefinder
{
   interface IImageProvider
   {
      public void DownloadImages(List<string> imageUrlSet);
   }
}
