﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;

namespace otus.html.imagefinder
{
   public class ImageDownloader : IImageProvider
   {
      string _folder;
      public ImageDownloader(string folder)
      {
         _folder = folder;
      }

      public void DownloadImages(List<string> imageUrlSet)
      {
         if (Directory.Exists(_folder))
         {
            foreach (string url in imageUrlSet)
            {
               string fileName = Path.GetFileName(url);
               using (WebClient client = new WebClient())
               {
                  try
                  {
                     byte[] data = client.DownloadData(url);
                     using (MemoryStream stream = new MemoryStream(data))
                     {
                        using (var img = Image.FromStream(stream))
                        {
                           img.Save(string.Concat(_folder, "\\", fileName, ".png"), ImageFormat.Png);
                        }
                     }
                     Console.WriteLine($"Image {fileName} downloaded");
                  }
                  catch (Exception ex)
                  {
                     Console.WriteLine($"Could not load image {fileName} from source {url}. Error: {ex.Message}");
                  }
               }
            }
         }
         else
         {
            Console.WriteLine($"Directory {_folder} does not exist.");
         }
         Console.WriteLine($"Download finished");
      }
   }
}
