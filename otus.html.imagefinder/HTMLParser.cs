﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace otus.html.imagefinder
{
   class HTMLParser
   {
      IImageProvider _provider;
      public string PageContent { get; private set; }
      public List<string> Images { get; private set; }
      public HTMLParser(IImageProvider provider, string url)
      {
         PageContent = GetContent(url);
         Images = GetImageUrls();
         _provider = provider;
      }

      public override string ToString()
         => PageContent;

      public static explicit operator string(HTMLParser parser)
         => parser.ToString();

      string GetContent(string url)
      {
         using WebClient client = new WebClient();
         string htmlCode = client.DownloadString(url);
         return htmlCode;
      }

      public List<string> GetImageUrls()
      {
         Regex urlRegex = new Regex("(i?)<img.+?src\\s?=\\s?[\"']?(?<image>[^\"'>\\s]+)");
         return urlRegex.Matches(PageContent).Select(s => s.Groups["image"].Value)
                                             .Distinct()
                                             .ToList();
      }

      public void DownloadImages()
      {
         _provider.DownloadImages(Images);
      }
   }
}
