﻿using System;

namespace otus.html.imagefinder
{
   class Program
   {
      static void Main(string[] args)
      {
         string url = @"https://otus.ru";
         ImageDownloader downloader = new ImageDownloader("D:\\Images");
         HTMLParser parser = new HTMLParser(downloader, url);
         foreach (string imageUrl in parser.Images)
         {
            Console.WriteLine(imageUrl);
         }


         //parser.DownloadImages();


         //Console.WriteLine(parser);
         Console.ReadKey();
      }
   }
}
